﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace patientenakte
{
    class ApiCaller
    {
        private String token = null;
        private String baseUrl;

        private object loginData;

        public ApiCaller(String username, String password, String baseUrl) {
            this.baseUrl = baseUrl;

            this.loginData = new {
                username = username,
                password = password
            };

            //setToken(this.baseUrl + "/login", createJson(loginData));
        }

        public bool login() {
            if (setToken(this.baseUrl + "/login", createJson(this.loginData)))
                return true;
            else
                return false;
        }

        public JContainer POST(string urlExtension, Object param) {
            return this.makePostCall(baseUrl + urlExtension, this.createJson(param));
        }

        public JContainer GET(string urlExtension) {
            return this.makeGetCall(baseUrl + urlExtension);
        }

        private String createJson(Object JsonObject) {
            return JsonConvert.SerializeObject(JsonObject);
        }

        private bool setToken(string url, string jsonContent) {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(jsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";

            using (Stream dataStream = request.GetRequestStream()) {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }

            JContainer resJson = this.makeApiCall(request);
            //Console.WriteLine(resJson);
            if (resJson != null) {
                this.token = resJson["token"].ToString();
                return true;
            }
            return false;
        }

        private JContainer makeGetCall(string url) {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            //request = addTokenToRequest(request);

            request.PreAuthenticate = true;
            request.Headers.Add("Authorization", "Bearer " + this.token);
            request.Accept = "application/json";

            return this.makeApiCall(request);
        }

        private JContainer makePostCall(string url, string jsonContent) {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";

            //add token
            request = addTokenToRequest(request);

            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(jsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";

            using (Stream dataStream = request.GetRequestStream()) {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }

            return this.makeApiCall(request);
        }

        private JContainer makeApiCall(HttpWebRequest request) {
            try {
                try {
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream())) {
                            return this.streamToJObject(reader);
                        }
                    }
                } catch (WebException e) {
                    if (e.Status == WebExceptionStatus.ProtocolError) {
                        using (var res = (HttpWebResponse)e.Response) {
                            using (var stream = res.GetResponseStream()) {
                                using (var reader = new StreamReader(stream, Encoding.GetEncoding("utf-8"))) {
                                    throw new ApiException(this.streamToJObject(reader));
                                }
                            }
                        }
                    }
                }
            } catch (ApiException e) {
                return null;
            }

            throw new Exception("Unexpected Error while calling API");
        }

        private JContainer streamToJObject(StreamReader reader) {
            var obj = reader.ReadToEnd();

            Console.WriteLine(obj.ToString());

            if (obj.ToString()[0] == '{') {
                JObject resJson = JObject.Parse(obj);
                return resJson;
            } else if (obj.ToString()[0] == '[') {
                JArray resJson = JArray.Parse(obj);
                return resJson;
            } else {
                throw new Exception("Unexpected json Format");
            }
        }


        private HttpWebRequest addTokenToRequest(HttpWebRequest request) {
            if (this.token != null) {
                request.PreAuthenticate = true;
                request.Headers.Add("Authorization", "Bearer " + this.token);
                request.Accept = "application/json";
                return request;
            }
            throw new Exception("Token not set");
        }
    }
}
