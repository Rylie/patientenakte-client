﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace patientenakte.Examination {
    public class ExaminationModel {
        private DateTime date;
        private String description;
        private String miscellaneous;
        private int id;

        public DateTime Date { get => date; set => date = value; }
        public string Description { get => description; set => description = value; }
        public string Miscellaneous { get => miscellaneous; set => miscellaneous = value; }
        public int Id { get => id; set => id = value; }

        public ExaminationModel(int id, DateTime date, string description, string miscellaneous) {
            this.id = id;
            this.date = date;
            this.description = description;
            this.miscellaneous = miscellaneous;
        }

        public ExaminationModel(DateTime date, string description, string miscellaneous) : this(-1, date, description, miscellaneous) { }
    }
}
