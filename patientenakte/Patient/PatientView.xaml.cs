﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace patientenakte.Patient {
    /// <summary>
    /// Interaktionslogik für PatientView.xaml
    /// </summary>
    public partial class PatientView : Page {
        PatientModelView patientModelView;

        DatabaseController databaseController;

        private List<PatientModel> patients;
        public List<PatientModel> Patients { get => patients; set => patients = value; }


        public PatientView(DatabaseController databaseController) {
            InitializeComponent();

            this.databaseController = databaseController;

            patientModelView = new PatientModelView(this.databaseController);
           
            this.Patients = patientModelView.loadPatientsFromDatabase();

            //Console.WriteLine(patients);

            //_DataGridPatients.ItemsSource = Patients;
            _DataGridPatients.Items.Add(patients[0]);
        }

    }
}
