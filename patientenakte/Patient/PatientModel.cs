﻿using patientenakte.Examination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace patientenakte.Patient
{
    public class PatientModel
    {
        private int id;
        private string healthInsurance;
        private string insuranceNumber;
        private string firstname;
        private string lastname;
        private string street;
        private string houseNumber;
        private string postCode;
        private string town;
        private DateTime birthday;
        private string phone;
        private string particularities;

        private List<ExaminationModel> examinations;

        public int Id { get => id; set => id = value; }
        public string HealthInsurance { get => healthInsurance; set => healthInsurance = value; }
        public string InsuranceNumber { get => insuranceNumber; set => insuranceNumber = value; }
        public string Firstname { get => firstname; set => firstname = value; }
        public string Lastname { get => lastname; set => lastname = value; }
        public string Street { get => street; set => street = value; }
        public string HouseNumber { get => houseNumber; set => houseNumber = value; }
        public string PostCode { get => postCode; set => postCode = value; }
        public string Town { get => town; set => town = value; }
        public DateTime Birthday { get => birthday; set => birthday = value; }
        public string Phone { get => phone; set => phone = value; }
        public string Particularities { get => particularities; set => particularities = value; }

        public PatientModel(int id, string healthInsurance, string insuranceNumber, string firstname, string lastname, string street, string houseNumber, string postCode, string town, DateTime birthday, string phone, string particularities, List<ExaminationModel> examinations)
        {
            this.Id = id;
            this.HealthInsurance = healthInsurance;
            this.InsuranceNumber = insuranceNumber;
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.Street = street;
            this.HouseNumber = houseNumber;
            this.PostCode = postCode;
            this.Town = town;
            this.Birthday = birthday;
            this.Phone = phone;
            this.Particularities = particularities;

            this.examinations = examinations;
        }

        public PatientModel(string healthInsurance, string insuranceNumber, string firstname, string lastname, string street, string houseNumber, string postCode, string town, DateTime birthday, string phone, string particularities) : this(-1, healthInsurance, insuranceNumber, firstname, lastname, street, houseNumber, postCode, town, birthday, phone, particularities, new List<ExaminationModel>()) { }
        public PatientModel(int id, string healthInsurance, string insuranceNumber, string firstname, string lastname, string street, string houseNumber, string postCode, string town, DateTime birthday, string phone, string particularities) : this(id, healthInsurance, insuranceNumber, firstname, lastname, street, houseNumber, postCode, town, birthday, phone, particularities, new List<ExaminationModel>()) { }


    }
}
