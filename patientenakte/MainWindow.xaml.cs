﻿using patientenakte.Patient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace patientenakte
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport("Kernel32")]
        public static extern void AllocConsole();

        private const string BASEURL = "https://api.patientenakte.nautha.de";

        //ApiCaller caller;
        DatabaseController databseController;


        public MainWindow() {
            InitializeComponent();
            AllocConsole();

            _textBoxUsername.Text = "e2fi1";
            _textBoxPassword.Password = "cssSael2019";
        }

        private void establishApiConnection(string username, string password) {
                this.databseController = new DatabaseController(username, password, BASEURL);

            if(this.databseController.login()) {
                LoggedInWindow window = new LoggedInWindow(this.databseController);
                window.ShowDialog();
            }
        }

        private void _buttonSubmit_Click(object sender, RoutedEventArgs e) {
            string username = _textBoxUsername.Text;
            string password = _textBoxPassword.Password;

            establishApiConnection(username, password);
        }
    }
}
