﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace patientenakte
{
    class ApiException : Exception
    {
        private string message;
        private string reason;
        private string errorId;

        public ApiException(JContainer error) : base(error["message"].ToString()) {
            this.message = error["message"].ToString();
            if (error["reason"] != null) {
                this.reason = error["reason"].ToString();
                MessageBox.Show(this.reason, this.message, MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (error["errorId"] != null) {
                this.errorId = error["errorId"].ToString();
                MessageBox.Show("Fehlercode: \n" + this.errorId, this.message, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public string Reason { get => reason; }
    }
}
