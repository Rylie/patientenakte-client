﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace patientenakte
{
    public class DatabaseController
    {
        private ApiCaller apiCaller;

        public DatabaseController(string username, string password, string baseUrl) {
            this.apiCaller = new ApiCaller(username, password, baseUrl);
            //try { 

            //apiCaller.POST("/patient", new {
            //    healthInsurance = "test",
            //    insuranceNumber = "1253",
            //    firstname = "Hans",
            //    lastname = "Rudi",
            //    street = "test",
            //    houseNumber = "8",
            //    postCode = "74336",
            //    town = "test",
            //    birthday = "1997-05-02",
            //    phone = "123456789",
            //    particularities = "test",
            //});

            //} catch (Exception e) {
            //    Console.WriteLine(e);
            //}

        }

        public bool login() {
            return this.apiCaller.login();
        }

        public JContainer getPatients() {
            try {
                return apiCaller.GET("/patients");
            } catch (Exception e) {
                Console.WriteLine(e);
            }
            return null;
        }
    }
}
